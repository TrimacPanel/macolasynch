﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacolaSynch
{
    class AccessIntegrityAlert
    {
        private string m_Sku;
        private string m_Description;

        public AccessIntegrityAlert(string parSku, string parDescription)
        {
            m_Sku = parSku;
            m_Description = parDescription;
        }

        public string SKU
        {
            get
            {
                return m_Sku;
            }
            set
            {
                m_Sku = value;
            }
        }

        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }
    }
}
